Rails.application.routes.draw do

  #use_doorkeeper

  #API routes
  namespace :api, except:[:new, :edit] do
    namespace :v1 do
          post 'users/auth/sign_in', to: 'users#sign_in'
          resources :enterprises
    end
  end

  #Application Routes
  root 'sessions#new'

  resources :users, only: [:new, :create]
  resources :sessions, only: [:new, :create]

  delete '/logout', to: 'sessions#destroy', as: :logout

  resources :enterprises
  resources :enterprise_types

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
