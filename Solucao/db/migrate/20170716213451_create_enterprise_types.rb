class CreateEnterpriseTypes < ActiveRecord::Migration[5.1]
  def change
    create_table :enterprise_types, id: false do |t|
      t.primary_key :enterprise_type_code
      t.string :name

      t.timestamps
    end
  end
end
