# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Doorkeeper::Application.create(name: "teste", redirect_uri:"urn:ietf:wg:oauth:2.0:oob")

User.create(email: "testeapple@ioasys.com.br", password: "12341234")
User.create(email: "lucasrizel@ioasys.com.br", password: "12345678")


['Agro',
'Aviation',
'Biotech',
'Eco',
'Ecommerce',
'Education',
'Fashion',
'Fintech',
'Food',
'Games',
'Health',
'IOT',
'Logistics',
'Media',
'Mining',
'Products',
'Real Estate',
'Service',
'Smart City',
'Social',
'Software',
'Technology',
'Tourism',
'Transport'].map {|n| EnterpriseType.create({name: n})}
