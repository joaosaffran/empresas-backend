json.extract! enterprise_type, :id, :enterprise_type_code, :name, :created_at, :updated_at
json.url enterprise_type_url(enterprise_type, format: :json)
