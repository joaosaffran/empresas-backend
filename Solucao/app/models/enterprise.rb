class Enterprise < ApplicationRecord
  include Filtrable

  belongs_to :enterprise_type
end
