class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  scope :nome, -> (nome) { where("name like ?", "%#{nome}%") }
  scope :enterprise_types, -> (type) { where({enterprise_type_id: type}) }
end
