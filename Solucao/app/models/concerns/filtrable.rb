module Filtrable
  extend ActiveSupport::Concern

  module ClassMethods
    def filter(filtering_params)
      results = self.all
      filtering_params.each do |key, value|
        if key == "name" && value.present?
          results = results.public_send(:nome, value)
        else
          results = results.public_send(key, value) if value.present?
        end
      end
      results
    end
  end
end
