module Encryption

  @@crypt = ActiveSupport::MessageEncryptor.new(Rails.application.secrets.secret_key_base[0..31])
  @@pattern = '|<\/>|'

  def encryptUser(email, password)
    @@crypt.encrypt_and_sign(email + @@pattern + password)
  end

  def decryptUser(data)
    aux = @@crypt.decrypt_and_verify(data)
    aux.split(@@pattern)
  end

end
