module ExceptionHandler

  extend ActiveSupport::Concern

  included do
    rescue_from ActiveRecord::RecordNotFound do |e|
      json_response({ message: e.message }, :not_found)
    end

    rescue_from ActiveRecord::RecordInvalid do |e|
      json_response({ message: e.message }, :unprocessable_entity)
    end

    rescue_from Exceptions::Unauthorized do |e|
      json_response("", :unauthorized)
    end

    rescue_from ActiveSupport::MessageVerifier::InvalidSignature do |e|
      json_response({message: "Invalid Client"}, :unauthorized)
    end
  end
end
