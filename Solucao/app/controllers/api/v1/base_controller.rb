module Api
  module V1
    class BaseController < ApplicationController
      include Response
      include ExceptionHandler
      include Encryption

      protect_from_forgery with: :null_session

      skip_before_action :authenticate

      before_action :destroy_session

      private

      def authorize
        token = request.headers['access-token']
        uid = request.headers['uid']
        client = request.headers['client']

        client = decryptUser(client)
        user = User.find_by(email: client[0])

        app = Doorkeeper::Application.find_by_uid(uid)

        app_token = app.access_tokens.where(token: token).first if app

        raise Exceptions::Unauthorized if (!app || !app_token || !(user && user.authenticate(client[1])))
      end

      def destroy_session
        request.session_options[:skip] = true
      end


    end
  end
end
