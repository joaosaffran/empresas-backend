module Api
  module V1
    class EnterprisesController < BaseController
      before_action :authorize

      def index
        json_response Enterprise.filter(params.slice(:enterprise_types, :name))
      end

      def show
        json_response Enterprise.find(params[:id])
      end

      def create
        json_response Enterprise.create(params[:empresa]), :created
      end

      def update
        json_response Enterprise.update(params[:id], params[:empresa])
      end

      def destroy
        json_response Enterprise.destroy(params[:id])
      end
    end
  end
end
