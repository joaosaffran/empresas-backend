module Api
  module V1
    class UsersController < BaseController


      def sign_in
        user = User.find_by(email: params[:email])
        if user && user.authenticate(params[:password])

          app = Doorkeeper::Application.first
          access_token = Doorkeeper::AccessToken.create!(
                         application: app,
                         resource_owner_id: user.id,
                         expires_in: 2.hours)

          user_credentials = encryptUser(params[:email], params[:password])

          response.headers['access-token'] = access_token.token
          response.headers['uid'] = app.uid
          response.headers['client'] = user_credentials

          json_response(user)
        else
          raise Exceptions::Unauthorized
        end
      end
    end
  end
end
