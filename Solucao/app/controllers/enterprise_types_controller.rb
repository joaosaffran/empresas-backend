class EnterpriseTypesController < ApplicationController
  before_action :set_enterprise_type, only: [:show, :edit, :update, :destroy]

  # GET /enterprise_types
  # GET /enterprise_types.json
  def index
    @enterprise_types = EnterpriseType.all
  end

  # GET /enterprise_types/1
  # GET /enterprise_types/1.json
  def show
  end

  # GET /enterprise_types/new
  def new
    @enterprise_type = EnterpriseType.new
  end

  # GET /enterprise_types/1/edit
  def edit
  end

  # POST /enterprise_types
  # POST /enterprise_types.json
  def create
    @enterprise_type = EnterpriseType.new(enterprise_type_params)

    respond_to do |format|
      if @enterprise_type.save
        format.html { redirect_to @enterprise_type, notice: 'Enterprise type was successfully created.' }
        format.json { render :show, status: :created, location: @enterprise_type }
      else
        format.html { render :new }
        format.json { render json: @enterprise_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /enterprise_types/1
  # PATCH/PUT /enterprise_types/1.json
  def update
    respond_to do |format|
      if @enterprise_type.update(enterprise_type_params)
        format.html { redirect_to @enterprise_type, notice: 'Enterprise type was successfully updated.' }
        format.json { render :show, status: :ok, location: @enterprise_type }
      else
        format.html { render :edit }
        format.json { render json: @enterprise_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /enterprise_types/1
  # DELETE /enterprise_types/1.json
  def destroy
    @enterprise_type.destroy
    respond_to do |format|
      format.html { redirect_to enterprise_types_url, notice: 'Enterprise type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_enterprise_type
      @enterprise_type = EnterpriseType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def enterprise_type_params
      params.require(:enterprise_type).permit(:enterprise_type_code, :name)
    end
end
